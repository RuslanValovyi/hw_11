Here's your homework:

Please see attached header, there are some classes declared with virtual base class.
You are to implement their methods and test them (minimal test program is attached in cpp file).
Don't forget about special rules for initialization lists in constructors.
Also pay attention to several methods with protected access. 
It simplifies code for some methods in HighFink class: e.g. if HighFink::showAll() would just runs Fink::showAll() and Manager::showAll(), 
it leads to double call of AbstractEmployee::showAll()

And some questions:
1) Why isn't assignment operation defined?
- The assignment operator generated by the compiler can repeatedly assign to a subobject of a virtual base class. 
  Or maybe only once. So it is not known how many times the copy of the 'AbstractEmployee' object will occur.
2) Why are showAll() and setAll() virtual?
- because in each inherited class different data will be set and showed
3) Why HighFink class doesn't have a data section?
- I don't see reason why it would not be possible to create a data section in the class HighFink
4) Why is one version of operator<<() is enough?
- because friends cannot be redefined in inherited class.


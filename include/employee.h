#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>
#include <string>

class AbstractEmployee {
private:
    std::string first_name_;
    std::string last_name_;
    std::string job_;

public:
    AbstractEmployee()
        : first_name_("none")
        , last_name_("none") 
        , job_("none") {};
    AbstractEmployee(const std::string& fn, const std::string& ln, const std::string& j) 
        : first_name_(fn)
        , last_name_(ln) 
        , job_(j) {};
    virtual void showAll() const { // prints all data with names
        std::cout << "first_name_: " << first_name_ << std::endl;
        std::cout << "last_name_: " << last_name_ << std::endl;
        std::cout << "job_: " << job_ << std::endl;
    };
    virtual void setAll() {  // gets data for class fields from user
        std::cout << "Enter first name: ";
        char fn[50] = "";
        std::cin.getline(fn, 50);
        first_name_ = fn;
        std::cout << "Enter last name: ";
        char ln[50] = "";
        std::cin.getline(ln, 50);
        last_name_ = ln;
        std::cout << "Enter job: ";
        char jb[50] = "";
        std::cin.getline(jb, 50);
        job_ = jb;
    };
    friend std::ostream& operator<<(std::ostream& os, const AbstractEmployee& e) { // prints first name & last name
        return os << e.first_name_ << "; " << e.last_name_;
    }
    virtual ~AbstractEmployee() = 0 ;
};

class Employee : public AbstractEmployee {
public:
    Employee()
        : AbstractEmployee() {};
    Employee(const std::string& fn, const std::string& ln, const std::string& j)
        : AbstractEmployee(fn, ln, j) {};
    virtual void showAll() const {AbstractEmployee::showAll();};
    virtual void setAll() {AbstractEmployee::setAll();};
};

class Manager : virtual public AbstractEmployee {
private:
    int in_charge_of_; // number of employees manager is in charge of

protected:
    int inChargeOf() const { return in_charge_of_; } // output
    void setInChargeOf(int employees){ in_charge_of_ = employees; } // input

public:
    Manager()
        : AbstractEmployee() {};
    Manager(const std::string& fn, const std::string& ln,const std::string& j, int ico = 0)
        : AbstractEmployee(fn, ln, j)
        , in_charge_of_(ico) {};
    Manager(const AbstractEmployee& e, int ico)
        : AbstractEmployee(e)
        , in_charge_of_(ico) {};
    Manager(const Manager& m)
        : AbstractEmployee(m) {}
    virtual void showAll() const {AbstractEmployee::showAll();};
    virtual void setAll() {AbstractEmployee::setAll();};
};

class Fink: virtual public AbstractEmployee { // strikebreaker
private:
    std::string reports_to_; // person, who he reports to

protected:
    const std::string reportsTo() const { return reports_to_; }
    void setReportsTo(const std::string head) { reports_to_ = head; }

public:
    Fink()
        : AbstractEmployee() {};
    Fink(const std::string& fn, const std::string& ln, const std::string& j, const std::string& rpo)
        : AbstractEmployee(fn, ln, j)
        , reports_to_(rpo) {};
    Fink(const AbstractEmployee& e, const std::string& rpo)
        : AbstractEmployee(e)
        , reports_to_(rpo) {};
    Fink(const Fink & e)
        : AbstractEmployee(e){};
    virtual void showAll() const {AbstractEmployee::showAll();};
    virtual void setAll() {AbstractEmployee::setAll();};
};

class HighFink: public Manager, public Fink { // fink supervisor
public:
    HighFink()
        : AbstractEmployee()
        , Manager()
        , Fink() {};
    HighFink(const std::string& fn, const std::string& ln,const std::string& j, const std::string& rpo, int ico)
        : AbstractEmployee(fn, ln, j)
        , Manager(fn, ln, j, ico)
        , Fink(fn, ln, j, rpo) {};
    HighFink(const AbstractEmployee& e, const std::string& rpo, int ico)
        : AbstractEmployee(e)
        , Manager(e, ico)
        , Fink(e, rpo) {};
    HighFink(const Fink& f, const std::string& rpo)
        : AbstractEmployee(f)
        , Fink(f, rpo) {};
    HighFink(const Manager& m, int ico)
        : AbstractEmployee(m)
        , Manager(m, ico) {};
    HighFink(const HighFink& h)
        : AbstractEmployee(h)
        , Manager(h)
        , Fink(h) {};
    virtual void showAll() const {AbstractEmployee::showAll();};
    virtual void setAll() {AbstractEmployee::setAll();};
};

#endif // EMPLOYEE_H
